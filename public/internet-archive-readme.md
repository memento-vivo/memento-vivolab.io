# Internet Archive Read Me

## Primary sources

* https://archive.org/
* https://twitter.com/internetarchive?lang=en
* https://www.linkedin.com/company/internet-archive/
* https://www.eventbrite.com/o/internet-archive-17912681148
* https://play.google.com/store/apps/details?id=internet.archivelibrary&hl=en_US
* https://www.youtube.com/channel/UCFa_X02QhJnP0FNpFAKyRRg
* https://www.flickr.com/photos/internetarchivebookimages/


## API

* https://archive.org/advancedsearch.php
* https://archive.org/help/aboutsearch.htm


## IA Developer resources

* https://archive.readme.io/
    * https://archive.readme.io/docs/getting-started
* https://github.com/internetarchive
* https://archive.org/services/docs/api/internetarchive/

## IA Apps

* https://archive.org/details/BookReader



## Secondary sources

* https://en.wikipedia.org/wiki/Internet_Archive
* https://www.loc.gov/item/2003541624/
* https://xkcd.com/2102/
* https://arxiv.org/abs/1309.4016
* https://www.archiveteam.org/index.php?title=Internet_Archive


## Third party apps

* https://iipimage.sourceforge.io/
* https://cran.r-project.org/web/packages/internetarchive/index.html
* https://ropensci.org/tutorials/internetarchive_tutorial/
* https://apps.apple.com/us/app/the-internet-archive-companion/id617179709
* https://tech.co/news/tools-to-help-you-search-the-archived-internet-2018-06

## Cheatsheets

* https://www.resdac.org/articles/using-internet-archive-find-historical-documents-and-downloadable-data-files



## Organizations of interest

* http://netpreserve.org/
* https://www.archiveteam.org/
* https://www.openapis.org



